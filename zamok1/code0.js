gdjs.NewSceneCode = {};
gdjs.NewSceneCode.GDbackgroundObjects1= [];
gdjs.NewSceneCode.GDbackgroundObjects2= [];
gdjs.NewSceneCode.GDbackgroundObjects3= [];
gdjs.NewSceneCode.GDbackgroundObjects4= [];
gdjs.NewSceneCode.GDbutton1Objects1= [];
gdjs.NewSceneCode.GDbutton1Objects2= [];
gdjs.NewSceneCode.GDbutton1Objects3= [];
gdjs.NewSceneCode.GDbutton1Objects4= [];
gdjs.NewSceneCode.GDbutton2Objects1= [];
gdjs.NewSceneCode.GDbutton2Objects2= [];
gdjs.NewSceneCode.GDbutton2Objects3= [];
gdjs.NewSceneCode.GDbutton2Objects4= [];
gdjs.NewSceneCode.GDlockObjects1= [];
gdjs.NewSceneCode.GDlockObjects2= [];
gdjs.NewSceneCode.GDlockObjects3= [];
gdjs.NewSceneCode.GDlockObjects4= [];
gdjs.NewSceneCode.GDlock2Objects1= [];
gdjs.NewSceneCode.GDlock2Objects2= [];
gdjs.NewSceneCode.GDlock2Objects3= [];
gdjs.NewSceneCode.GDlock2Objects4= [];
gdjs.NewSceneCode.GDopenButtonObjects1= [];
gdjs.NewSceneCode.GDopenButtonObjects2= [];
gdjs.NewSceneCode.GDopenButtonObjects3= [];
gdjs.NewSceneCode.GDopenButtonObjects4= [];
gdjs.NewSceneCode.GDlamp1Objects1= [];
gdjs.NewSceneCode.GDlamp1Objects2= [];
gdjs.NewSceneCode.GDlamp1Objects3= [];
gdjs.NewSceneCode.GDlamp1Objects4= [];
gdjs.NewSceneCode.GDlamp2Objects1= [];
gdjs.NewSceneCode.GDlamp2Objects2= [];
gdjs.NewSceneCode.GDlamp2Objects3= [];
gdjs.NewSceneCode.GDlamp2Objects4= [];
gdjs.NewSceneCode.GDlamp3Objects1= [];
gdjs.NewSceneCode.GDlamp3Objects2= [];
gdjs.NewSceneCode.GDlamp3Objects3= [];
gdjs.NewSceneCode.GDlamp3Objects4= [];
gdjs.NewSceneCode.GDnumberObjects1= [];
gdjs.NewSceneCode.GDnumberObjects2= [];
gdjs.NewSceneCode.GDnumberObjects3= [];
gdjs.NewSceneCode.GDnumberObjects4= [];
gdjs.NewSceneCode.GDnumber2Objects1= [];
gdjs.NewSceneCode.GDnumber2Objects2= [];
gdjs.NewSceneCode.GDnumber2Objects3= [];
gdjs.NewSceneCode.GDnumber2Objects4= [];
gdjs.NewSceneCode.GDprutya_95horizontalObjects1= [];
gdjs.NewSceneCode.GDprutya_95horizontalObjects2= [];
gdjs.NewSceneCode.GDprutya_95horizontalObjects3= [];
gdjs.NewSceneCode.GDprutya_95horizontalObjects4= [];
gdjs.NewSceneCode.GDprutya_95verticalObjects1= [];
gdjs.NewSceneCode.GDprutya_95verticalObjects2= [];
gdjs.NewSceneCode.GDprutya_95verticalObjects3= [];
gdjs.NewSceneCode.GDprutya_95verticalObjects4= [];
gdjs.NewSceneCode.GDtumbler1Objects1= [];
gdjs.NewSceneCode.GDtumbler1Objects2= [];
gdjs.NewSceneCode.GDtumbler1Objects3= [];
gdjs.NewSceneCode.GDtumbler1Objects4= [];
gdjs.NewSceneCode.GDtumbler2Objects1= [];
gdjs.NewSceneCode.GDtumbler2Objects2= [];
gdjs.NewSceneCode.GDtumbler2Objects3= [];
gdjs.NewSceneCode.GDtumbler2Objects4= [];
gdjs.NewSceneCode.GDtumbler3Objects1= [];
gdjs.NewSceneCode.GDtumbler3Objects2= [];
gdjs.NewSceneCode.GDtumbler3Objects3= [];
gdjs.NewSceneCode.GDtumbler3Objects4= [];
gdjs.NewSceneCode.GDupObjects1= [];
gdjs.NewSceneCode.GDupObjects2= [];
gdjs.NewSceneCode.GDupObjects3= [];
gdjs.NewSceneCode.GDupObjects4= [];
gdjs.NewSceneCode.GDup2Objects1= [];
gdjs.NewSceneCode.GDup2Objects2= [];
gdjs.NewSceneCode.GDup2Objects3= [];
gdjs.NewSceneCode.GDup2Objects4= [];
gdjs.NewSceneCode.GDwheelObjects1= [];
gdjs.NewSceneCode.GDwheelObjects2= [];
gdjs.NewSceneCode.GDwheelObjects3= [];
gdjs.NewSceneCode.GDwheelObjects4= [];

gdjs.NewSceneCode.conditionTrue_0 = {val:false};
gdjs.NewSceneCode.condition0IsTrue_0 = {val:false};
gdjs.NewSceneCode.condition1IsTrue_0 = {val:false};
gdjs.NewSceneCode.condition2IsTrue_0 = {val:false};
gdjs.NewSceneCode.condition3IsTrue_0 = {val:false};
gdjs.NewSceneCode.condition4IsTrue_0 = {val:false};


gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDbutton1Objects1Objects = Hashtable.newFrom({"button1": gdjs.NewSceneCode.GDbutton1Objects1});gdjs.NewSceneCode.eventsList0x6e2300 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDlock2Objects2.createFrom(runtimeScene.getObjects("lock2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlock2Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlock2Objects2[i].getAnimation() == 0 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlock2Objects2[k] = gdjs.NewSceneCode.GDlock2Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlock2Objects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
}

}


{

gdjs.NewSceneCode.GDlock2Objects1.createFrom(runtimeScene.getObjects("lock2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlock2Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlock2Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlock2Objects1[k] = gdjs.NewSceneCode.GDlock2Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlock2Objects1.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
gdjs.NewSceneCode.GDprutya_95horizontalObjects1.createFrom(runtimeScene.getObjects("prutya_horizontal"));
{for(var i = 0, len = gdjs.NewSceneCode.GDprutya_95horizontalObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDprutya_95horizontalObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x6e2300
gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDbutton1Objects1Objects = Hashtable.newFrom({"button1": gdjs.NewSceneCode.GDbutton1Objects1});gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDbutton2Objects1Objects = Hashtable.newFrom({"button2": gdjs.NewSceneCode.GDbutton2Objects1});gdjs.NewSceneCode.eventsList0x6e2c20 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDlockObjects2.createFrom(runtimeScene.getObjects("lock"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlockObjects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlockObjects2[i].getAnimation() == 0 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlockObjects2[k] = gdjs.NewSceneCode.GDlockObjects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlockObjects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
}

}


{

gdjs.NewSceneCode.GDlockObjects1.createFrom(runtimeScene.getObjects("lock"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlockObjects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlockObjects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlockObjects1[k] = gdjs.NewSceneCode.GDlockObjects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlockObjects1.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
gdjs.NewSceneCode.GDprutya_95verticalObjects1.createFrom(runtimeScene.getObjects("prutya_vertical"));
{for(var i = 0, len = gdjs.NewSceneCode.GDprutya_95verticalObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDprutya_95verticalObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x6e2c20
gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDbutton2Objects1Objects = Hashtable.newFrom({"button2": gdjs.NewSceneCode.GDbutton2Objects1});gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDupObjects1Objects = Hashtable.newFrom({"up": gdjs.NewSceneCode.GDupObjects1});gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDupObjects1Objects = Hashtable.newFrom({"up": gdjs.NewSceneCode.GDupObjects1});gdjs.NewSceneCode.eventsList0x6e38f0 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDnumberObjects2.createFrom(runtimeScene.getObjects("number"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDnumberObjects2[i].getAnimation() <= 8 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDnumberObjects2[k] = gdjs.NewSceneCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDnumberObjects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDnumberObjects2[i].setAnimation(gdjs.NewSceneCode.GDnumberObjects2[i].getAnimation() + (1));
}
}}

}


{

gdjs.NewSceneCode.GDnumberObjects2.createFrom(runtimeScene.getObjects("number"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDnumberObjects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDnumberObjects2[i].getAnimation() == 9 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDnumberObjects2[k] = gdjs.NewSceneCode.GDnumberObjects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDnumberObjects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDnumberObjects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDnumberObjects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDnumberObjects2[i].setAnimation(0);
}
}}

}


{

gdjs.NewSceneCode.GDnumberObjects1.createFrom(runtimeScene.getObjects("number"));
gdjs.NewSceneCode.GDnumber2Objects1.createFrom(runtimeScene.getObjects("number2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDnumberObjects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDnumberObjects1[i].getAnimation() == 5 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDnumberObjects1[k] = gdjs.NewSceneCode.GDnumberObjects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDnumberObjects1.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDnumber2Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDnumber2Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition1IsTrue_0.val = true;
        gdjs.NewSceneCode.GDnumber2Objects1[k] = gdjs.NewSceneCode.GDnumber2Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDnumber2Objects1.length = k;}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
gdjs.NewSceneCode.GDlock2Objects1.createFrom(runtimeScene.getObjects("lock2"));
{for(var i = 0, len = gdjs.NewSceneCode.GDlock2Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlock2Objects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x6e38f0
gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDup2Objects1Objects = Hashtable.newFrom({"up2": gdjs.NewSceneCode.GDup2Objects1});gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDup2Objects1Objects = Hashtable.newFrom({"up2": gdjs.NewSceneCode.GDup2Objects1});gdjs.NewSceneCode.eventsList0x6e44f0 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDnumber2Objects2.createFrom(runtimeScene.getObjects("number2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDnumber2Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDnumber2Objects2[i].getAnimation() <= 8 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDnumber2Objects2[k] = gdjs.NewSceneCode.GDnumber2Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDnumber2Objects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDnumber2Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDnumber2Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDnumber2Objects2[i].setAnimation(gdjs.NewSceneCode.GDnumber2Objects2[i].getAnimation() + (1));
}
}}

}


{

gdjs.NewSceneCode.GDnumber2Objects2.createFrom(runtimeScene.getObjects("number2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDnumber2Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDnumber2Objects2[i].getAnimation() == 9 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDnumber2Objects2[k] = gdjs.NewSceneCode.GDnumber2Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDnumber2Objects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDnumber2Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDnumber2Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDnumber2Objects2[i].setAnimation(0);
}
}}

}


{

gdjs.NewSceneCode.GDnumberObjects1.createFrom(runtimeScene.getObjects("number"));
gdjs.NewSceneCode.GDnumber2Objects1.createFrom(runtimeScene.getObjects("number2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDnumberObjects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDnumberObjects1[i].getAnimation() == 5 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDnumberObjects1[k] = gdjs.NewSceneCode.GDnumberObjects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDnumberObjects1.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDnumber2Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDnumber2Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition1IsTrue_0.val = true;
        gdjs.NewSceneCode.GDnumber2Objects1[k] = gdjs.NewSceneCode.GDnumber2Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDnumber2Objects1.length = k;}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
gdjs.NewSceneCode.GDlock2Objects1.createFrom(runtimeScene.getObjects("lock2"));
{for(var i = 0, len = gdjs.NewSceneCode.GDlock2Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlock2Objects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x6e44f0
gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDwheelObjects1Objects = Hashtable.newFrom({"wheel": gdjs.NewSceneCode.GDwheelObjects1});gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDtumbler1Objects1Objects = Hashtable.newFrom({"tumbler1": gdjs.NewSceneCode.GDtumbler1Objects1});gdjs.NewSceneCode.eventsList0x6e56b8 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDlamp3Objects3.createFrom(runtimeScene.getObjects("lamp3"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp3Objects3.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp3Objects3[i].getAnimation() <= 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp3Objects3[k] = gdjs.NewSceneCode.GDlamp3Objects3[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp3Objects3.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp3Objects3 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp3Objects3.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp3Objects3[i].setAnimation(gdjs.NewSceneCode.GDlamp3Objects3[i].getAnimation() + (1));
}
}}

}


{

gdjs.NewSceneCode.GDlamp3Objects2.createFrom(runtimeScene.getObjects("lamp3"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp3Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp3Objects2[i].getAnimation() == 2 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp3Objects2[k] = gdjs.NewSceneCode.GDlamp3Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp3Objects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp3Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp3Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp3Objects2[i].setAnimation(0);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x6e56b8
gdjs.NewSceneCode.eventsList0x6e52a0 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDtumbler1Objects2.createFrom(gdjs.NewSceneCode.GDtumbler1Objects1);


gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDtumbler1Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDtumbler1Objects2[i].getAnimation() <= 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDtumbler1Objects2[k] = gdjs.NewSceneCode.GDtumbler1Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDtumbler1Objects2.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDtumbler1Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDtumbler1Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDtumbler1Objects2[i].setAnimation(gdjs.NewSceneCode.GDtumbler1Objects2[i].getAnimation() + (1));
}
}}

}


{

gdjs.NewSceneCode.GDtumbler1Objects2.createFrom(gdjs.NewSceneCode.GDtumbler1Objects1);


gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDtumbler1Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDtumbler1Objects2[i].getAnimation() == 2 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDtumbler1Objects2[k] = gdjs.NewSceneCode.GDtumbler1Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDtumbler1Objects2.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDtumbler1Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDtumbler1Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDtumbler1Objects2[i].setAnimation(0);
}
}
{ //Subevents
gdjs.NewSceneCode.eventsList0x6e56b8(runtimeScene);} //End of subevents
}

}


{

gdjs.NewSceneCode.GDlamp1Objects1.createFrom(runtimeScene.getObjects("lamp1"));
gdjs.NewSceneCode.GDlamp2Objects1.createFrom(runtimeScene.getObjects("lamp2"));
gdjs.NewSceneCode.GDlamp3Objects1.createFrom(runtimeScene.getObjects("lamp3"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
gdjs.NewSceneCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp1Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp1Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp1Objects1[k] = gdjs.NewSceneCode.GDlamp1Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp1Objects1.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp2Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp2Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition1IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp2Objects1[k] = gdjs.NewSceneCode.GDlamp2Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp2Objects1.length = k;}if ( gdjs.NewSceneCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp3Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp3Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition2IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp3Objects1[k] = gdjs.NewSceneCode.GDlamp3Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp3Objects1.length = k;}}
}
if (gdjs.NewSceneCode.condition2IsTrue_0.val) {
gdjs.NewSceneCode.GDlockObjects1.createFrom(runtimeScene.getObjects("lock"));
{for(var i = 0, len = gdjs.NewSceneCode.GDlockObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlockObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x6e52a0
gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDtumbler2Objects1Objects = Hashtable.newFrom({"tumbler2": gdjs.NewSceneCode.GDtumbler2Objects1});gdjs.NewSceneCode.eventsList0x6e62e8 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDlamp3Objects3.createFrom(runtimeScene.getObjects("lamp3"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp3Objects3.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp3Objects3[i].getAnimation() <= 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp3Objects3[k] = gdjs.NewSceneCode.GDlamp3Objects3[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp3Objects3.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp3Objects3 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp3Objects3.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp3Objects3[i].setAnimation(gdjs.NewSceneCode.GDlamp3Objects3[i].getAnimation() + (1));
}
}}

}


{

gdjs.NewSceneCode.GDlamp3Objects3.createFrom(runtimeScene.getObjects("lamp3"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp3Objects3.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp3Objects3[i].getAnimation() == 2 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp3Objects3[k] = gdjs.NewSceneCode.GDlamp3Objects3[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp3Objects3.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp3Objects3 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp3Objects3.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp3Objects3[i].setAnimation(0);
}
}}

}


{

gdjs.NewSceneCode.GDlamp1Objects3.createFrom(runtimeScene.getObjects("lamp1"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp1Objects3.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp1Objects3[i].getAnimation() <= 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp1Objects3[k] = gdjs.NewSceneCode.GDlamp1Objects3[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp1Objects3.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp1Objects3 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp1Objects3.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp1Objects3[i].setAnimation(gdjs.NewSceneCode.GDlamp1Objects3[i].getAnimation() + (1));
}
}}

}


{

gdjs.NewSceneCode.GDlamp1Objects2.createFrom(runtimeScene.getObjects("lamp1"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp1Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp1Objects2[i].getAnimation() == 2 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp1Objects2[k] = gdjs.NewSceneCode.GDlamp1Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp1Objects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp1Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp1Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp1Objects2[i].setAnimation(0);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x6e62e8
gdjs.NewSceneCode.eventsList0x6e6198 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDtumbler2Objects2.createFrom(gdjs.NewSceneCode.GDtumbler2Objects1);


gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDtumbler2Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDtumbler2Objects2[i].getAnimation() <= 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDtumbler2Objects2[k] = gdjs.NewSceneCode.GDtumbler2Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDtumbler2Objects2.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDtumbler2Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDtumbler2Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDtumbler2Objects2[i].setAnimation(gdjs.NewSceneCode.GDtumbler2Objects2[i].getAnimation() + (1));
}
}
{ //Subevents
gdjs.NewSceneCode.eventsList0x6e62e8(runtimeScene);} //End of subevents
}

}


{

gdjs.NewSceneCode.GDtumbler2Objects2.createFrom(gdjs.NewSceneCode.GDtumbler2Objects1);


gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDtumbler2Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDtumbler2Objects2[i].getAnimation() == 2 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDtumbler2Objects2[k] = gdjs.NewSceneCode.GDtumbler2Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDtumbler2Objects2.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDtumbler2Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDtumbler2Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDtumbler2Objects2[i].setAnimation(0);
}
}}

}


{

gdjs.NewSceneCode.GDlamp1Objects1.createFrom(runtimeScene.getObjects("lamp1"));
gdjs.NewSceneCode.GDlamp2Objects1.createFrom(runtimeScene.getObjects("lamp2"));
gdjs.NewSceneCode.GDlamp3Objects1.createFrom(runtimeScene.getObjects("lamp3"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
gdjs.NewSceneCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp1Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp1Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp1Objects1[k] = gdjs.NewSceneCode.GDlamp1Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp1Objects1.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp2Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp2Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition1IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp2Objects1[k] = gdjs.NewSceneCode.GDlamp2Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp2Objects1.length = k;}if ( gdjs.NewSceneCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp3Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp3Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition2IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp3Objects1[k] = gdjs.NewSceneCode.GDlamp3Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp3Objects1.length = k;}}
}
if (gdjs.NewSceneCode.condition2IsTrue_0.val) {
gdjs.NewSceneCode.GDlockObjects1.createFrom(runtimeScene.getObjects("lock"));
{for(var i = 0, len = gdjs.NewSceneCode.GDlockObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlockObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x6e6198
gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDtumbler3Objects1Objects = Hashtable.newFrom({"tumbler3": gdjs.NewSceneCode.GDtumbler3Objects1});gdjs.NewSceneCode.eventsList0x69a280 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDlamp2Objects3.createFrom(runtimeScene.getObjects("lamp2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp2Objects3.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp2Objects3[i].getAnimation() <= 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp2Objects3[k] = gdjs.NewSceneCode.GDlamp2Objects3[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp2Objects3.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp2Objects3 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp2Objects3.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp2Objects3[i].setAnimation(gdjs.NewSceneCode.GDlamp2Objects3[i].getAnimation() + (1));
}
}}

}


{

gdjs.NewSceneCode.GDlamp2Objects3.createFrom(runtimeScene.getObjects("lamp2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp2Objects3.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp2Objects3[i].getAnimation() == 2 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp2Objects3[k] = gdjs.NewSceneCode.GDlamp2Objects3[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp2Objects3.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp2Objects3 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp2Objects3.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp2Objects3[i].setAnimation(0);
}
}}

}


{

gdjs.NewSceneCode.GDlamp1Objects3.createFrom(runtimeScene.getObjects("lamp1"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp1Objects3.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp1Objects3[i].getAnimation() <= 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp1Objects3[k] = gdjs.NewSceneCode.GDlamp1Objects3[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp1Objects3.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp1Objects3 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp1Objects3.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp1Objects3[i].setAnimation(gdjs.NewSceneCode.GDlamp1Objects3[i].getAnimation() + (1));
}
}}

}


{

gdjs.NewSceneCode.GDlamp1Objects2.createFrom(runtimeScene.getObjects("lamp1"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp1Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp1Objects2[i].getAnimation() == 2 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp1Objects2[k] = gdjs.NewSceneCode.GDlamp1Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp1Objects2.length = k;}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDlamp1Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp1Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp1Objects2[i].setAnimation(0);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x69a280
gdjs.NewSceneCode.eventsList0x738c00 = function(runtimeScene) {

{

gdjs.NewSceneCode.GDtumbler3Objects2.createFrom(gdjs.NewSceneCode.GDtumbler3Objects1);


gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDtumbler3Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDtumbler3Objects2[i].getAnimation() <= 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDtumbler3Objects2[k] = gdjs.NewSceneCode.GDtumbler3Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDtumbler3Objects2.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDtumbler3Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDtumbler3Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDtumbler3Objects2[i].setAnimation(gdjs.NewSceneCode.GDtumbler3Objects2[i].getAnimation() + (1));
}
}
{ //Subevents
gdjs.NewSceneCode.eventsList0x69a280(runtimeScene);} //End of subevents
}

}


{

gdjs.NewSceneCode.GDtumbler3Objects2.createFrom(gdjs.NewSceneCode.GDtumbler3Objects1);


gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDtumbler3Objects2.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDtumbler3Objects2[i].getAnimation() == 2 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDtumbler3Objects2[k] = gdjs.NewSceneCode.GDtumbler3Objects2[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDtumbler3Objects2.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDtumbler3Objects2 */
{for(var i = 0, len = gdjs.NewSceneCode.GDtumbler3Objects2.length ;i < len;++i) {
    gdjs.NewSceneCode.GDtumbler3Objects2[i].setAnimation(0);
}
}}

}


{

gdjs.NewSceneCode.GDlamp1Objects1.createFrom(runtimeScene.getObjects("lamp1"));
gdjs.NewSceneCode.GDlamp2Objects1.createFrom(runtimeScene.getObjects("lamp2"));
gdjs.NewSceneCode.GDlamp3Objects1.createFrom(runtimeScene.getObjects("lamp3"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
gdjs.NewSceneCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp1Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp1Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp1Objects1[k] = gdjs.NewSceneCode.GDlamp1Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp1Objects1.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp2Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp2Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition1IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp2Objects1[k] = gdjs.NewSceneCode.GDlamp2Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp2Objects1.length = k;}if ( gdjs.NewSceneCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDlamp3Objects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDlamp3Objects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition2IsTrue_0.val = true;
        gdjs.NewSceneCode.GDlamp3Objects1[k] = gdjs.NewSceneCode.GDlamp3Objects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDlamp3Objects1.length = k;}}
}
if (gdjs.NewSceneCode.condition2IsTrue_0.val) {
gdjs.NewSceneCode.GDlockObjects1.createFrom(runtimeScene.getObjects("lock"));
{for(var i = 0, len = gdjs.NewSceneCode.GDlockObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlockObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.NewSceneCode.eventsList0x738c00
gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDopenButtonObjects1Objects = Hashtable.newFrom({"openButton": gdjs.NewSceneCode.GDopenButtonObjects1});gdjs.NewSceneCode.eventsList0xb2358 = function(runtimeScene) {

{


gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {
gdjs.NewSceneCode.GDlamp1Objects1.createFrom(runtimeScene.getObjects("lamp1"));
gdjs.NewSceneCode.GDlamp3Objects1.createFrom(runtimeScene.getObjects("lamp3"));
gdjs.NewSceneCode.GDtumbler2Objects1.createFrom(runtimeScene.getObjects("tumbler2"));
{for(var i = 0, len = gdjs.NewSceneCode.GDlamp1Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp1Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.NewSceneCode.GDlamp3Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDlamp3Objects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.NewSceneCode.GDtumbler2Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDtumbler2Objects1[i].setAnimation(1);
}
}}

}


{



}


{

gdjs.NewSceneCode.GDbutton1Objects1.createFrom(runtimeScene.getObjects("button1"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDbutton1Objects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDbutton1Objects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDbutton1Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDbutton1Objects1[i].setAnimation(1);
}
}
{ //Subevents
gdjs.NewSceneCode.eventsList0x6e2300(runtimeScene);} //End of subevents
}

}


{

gdjs.NewSceneCode.GDbutton1Objects1.createFrom(runtimeScene.getObjects("button1"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDbutton1Objects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDbutton1Objects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDbutton1Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDbutton1Objects1[i].setAnimation(0);
}
}}

}


{

gdjs.NewSceneCode.GDbutton2Objects1.createFrom(runtimeScene.getObjects("button2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDbutton2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDbutton2Objects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDbutton2Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDbutton2Objects1[i].setAnimation(1);
}
}
{ //Subevents
gdjs.NewSceneCode.eventsList0x6e2c20(runtimeScene);} //End of subevents
}

}


{

gdjs.NewSceneCode.GDbutton2Objects1.createFrom(runtimeScene.getObjects("button2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDbutton2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDbutton2Objects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDbutton2Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDbutton2Objects1[i].setAnimation(0);
}
}}

}


{



}


{

gdjs.NewSceneCode.GDupObjects1.createFrom(runtimeScene.getObjects("up"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDupObjects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDupObjects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDupObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDupObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.NewSceneCode.GDupObjects1.createFrom(runtimeScene.getObjects("up"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDupObjects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDupObjects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDupObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDupObjects1[i].setAnimation(0);
}
}
{ //Subevents
gdjs.NewSceneCode.eventsList0x6e38f0(runtimeScene);} //End of subevents
}

}


{

gdjs.NewSceneCode.GDup2Objects1.createFrom(runtimeScene.getObjects("up2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDup2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDup2Objects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDup2Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDup2Objects1[i].setAnimation(1);
}
}}

}


{

gdjs.NewSceneCode.GDup2Objects1.createFrom(runtimeScene.getObjects("up2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDup2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDup2Objects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDup2Objects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDup2Objects1[i].setAnimation(0);
}
}
{ //Subevents
gdjs.NewSceneCode.eventsList0x6e44f0(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.NewSceneCode.GDwheelObjects1.createFrom(runtimeScene.getObjects("wheel"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDwheelObjects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.NewSceneCode.condition1IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDwheelObjects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDwheelObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDwheelObjects1[i].setAngle(gdjs.NewSceneCode.GDwheelObjects1[i].getAngle() + (45));
}
}}

}


{



}


{

gdjs.NewSceneCode.GDtumbler1Objects1.createFrom(runtimeScene.getObjects("tumbler1"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDtumbler1Objects1Objects, runtimeScene, true, false);
}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.NewSceneCode.eventsList0x6e52a0(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.NewSceneCode.GDtumbler2Objects1.createFrom(runtimeScene.getObjects("tumbler2"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDtumbler2Objects1Objects, runtimeScene, true, false);
}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.NewSceneCode.eventsList0x6e6198(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.NewSceneCode.GDtumbler3Objects1.createFrom(runtimeScene.getObjects("tumbler3"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDtumbler3Objects1Objects, runtimeScene, true, false);
}if (gdjs.NewSceneCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.NewSceneCode.eventsList0x738c00(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.NewSceneCode.GDopenButtonObjects1.createFrom(runtimeScene.getObjects("openButton"));
gdjs.NewSceneCode.GDprutya_95horizontalObjects1.createFrom(runtimeScene.getObjects("prutya_horizontal"));
gdjs.NewSceneCode.GDprutya_95verticalObjects1.createFrom(runtimeScene.getObjects("prutya_vertical"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
gdjs.NewSceneCode.condition2IsTrue_0.val = false;
gdjs.NewSceneCode.condition3IsTrue_0.val = false;
{
gdjs.NewSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.NewSceneCode.mapOfGDgdjs_46NewSceneCode_46GDopenButtonObjects1Objects, runtimeScene, true, false);
}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDprutya_95horizontalObjects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDprutya_95horizontalObjects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition1IsTrue_0.val = true;
        gdjs.NewSceneCode.GDprutya_95horizontalObjects1[k] = gdjs.NewSceneCode.GDprutya_95horizontalObjects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDprutya_95horizontalObjects1.length = k;}if ( gdjs.NewSceneCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDprutya_95verticalObjects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDprutya_95verticalObjects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition2IsTrue_0.val = true;
        gdjs.NewSceneCode.GDprutya_95verticalObjects1[k] = gdjs.NewSceneCode.GDprutya_95verticalObjects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDprutya_95verticalObjects1.length = k;}if ( gdjs.NewSceneCode.condition2IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition3IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
}
}
if (gdjs.NewSceneCode.condition3IsTrue_0.val) {
/* Reuse gdjs.NewSceneCode.GDopenButtonObjects1 */
{for(var i = 0, len = gdjs.NewSceneCode.GDopenButtonObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDopenButtonObjects1[i].setAnimation(1);
}
}}

}


{

gdjs.NewSceneCode.GDprutya_95horizontalObjects1.createFrom(runtimeScene.getObjects("prutya_horizontal"));
gdjs.NewSceneCode.GDprutya_95verticalObjects1.createFrom(runtimeScene.getObjects("prutya_vertical"));

gdjs.NewSceneCode.condition0IsTrue_0.val = false;
gdjs.NewSceneCode.condition1IsTrue_0.val = false;
gdjs.NewSceneCode.condition2IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDprutya_95horizontalObjects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDprutya_95horizontalObjects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition0IsTrue_0.val = true;
        gdjs.NewSceneCode.GDprutya_95horizontalObjects1[k] = gdjs.NewSceneCode.GDprutya_95horizontalObjects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDprutya_95horizontalObjects1.length = k;}if ( gdjs.NewSceneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.NewSceneCode.GDprutya_95verticalObjects1.length;i<l;++i) {
    if ( gdjs.NewSceneCode.GDprutya_95verticalObjects1[i].getAnimation() == 1 ) {
        gdjs.NewSceneCode.condition1IsTrue_0.val = true;
        gdjs.NewSceneCode.GDprutya_95verticalObjects1[k] = gdjs.NewSceneCode.GDprutya_95verticalObjects1[i];
        ++k;
    }
}
gdjs.NewSceneCode.GDprutya_95verticalObjects1.length = k;}if ( gdjs.NewSceneCode.condition1IsTrue_0.val ) {
{
gdjs.NewSceneCode.condition2IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
}
if (gdjs.NewSceneCode.condition2IsTrue_0.val) {
gdjs.NewSceneCode.GDopenButtonObjects1.createFrom(runtimeScene.getObjects("openButton"));
{for(var i = 0, len = gdjs.NewSceneCode.GDopenButtonObjects1.length ;i < len;++i) {
    gdjs.NewSceneCode.GDopenButtonObjects1[i].setAnimation(0);
}
}{gdjs.evtTools.window.openURL("http://www.pethealthnetwork.com/sites/default/files/why-should-i-spay-my-new-kitten-138101629.jpg", runtimeScene);
}}

}


}; //End of gdjs.NewSceneCode.eventsList0xb2358


gdjs.NewSceneCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.NewSceneCode.GDbackgroundObjects1.length = 0;
gdjs.NewSceneCode.GDbackgroundObjects2.length = 0;
gdjs.NewSceneCode.GDbackgroundObjects3.length = 0;
gdjs.NewSceneCode.GDbackgroundObjects4.length = 0;
gdjs.NewSceneCode.GDbutton1Objects1.length = 0;
gdjs.NewSceneCode.GDbutton1Objects2.length = 0;
gdjs.NewSceneCode.GDbutton1Objects3.length = 0;
gdjs.NewSceneCode.GDbutton1Objects4.length = 0;
gdjs.NewSceneCode.GDbutton2Objects1.length = 0;
gdjs.NewSceneCode.GDbutton2Objects2.length = 0;
gdjs.NewSceneCode.GDbutton2Objects3.length = 0;
gdjs.NewSceneCode.GDbutton2Objects4.length = 0;
gdjs.NewSceneCode.GDlockObjects1.length = 0;
gdjs.NewSceneCode.GDlockObjects2.length = 0;
gdjs.NewSceneCode.GDlockObjects3.length = 0;
gdjs.NewSceneCode.GDlockObjects4.length = 0;
gdjs.NewSceneCode.GDlock2Objects1.length = 0;
gdjs.NewSceneCode.GDlock2Objects2.length = 0;
gdjs.NewSceneCode.GDlock2Objects3.length = 0;
gdjs.NewSceneCode.GDlock2Objects4.length = 0;
gdjs.NewSceneCode.GDopenButtonObjects1.length = 0;
gdjs.NewSceneCode.GDopenButtonObjects2.length = 0;
gdjs.NewSceneCode.GDopenButtonObjects3.length = 0;
gdjs.NewSceneCode.GDopenButtonObjects4.length = 0;
gdjs.NewSceneCode.GDlamp1Objects1.length = 0;
gdjs.NewSceneCode.GDlamp1Objects2.length = 0;
gdjs.NewSceneCode.GDlamp1Objects3.length = 0;
gdjs.NewSceneCode.GDlamp1Objects4.length = 0;
gdjs.NewSceneCode.GDlamp2Objects1.length = 0;
gdjs.NewSceneCode.GDlamp2Objects2.length = 0;
gdjs.NewSceneCode.GDlamp2Objects3.length = 0;
gdjs.NewSceneCode.GDlamp2Objects4.length = 0;
gdjs.NewSceneCode.GDlamp3Objects1.length = 0;
gdjs.NewSceneCode.GDlamp3Objects2.length = 0;
gdjs.NewSceneCode.GDlamp3Objects3.length = 0;
gdjs.NewSceneCode.GDlamp3Objects4.length = 0;
gdjs.NewSceneCode.GDnumberObjects1.length = 0;
gdjs.NewSceneCode.GDnumberObjects2.length = 0;
gdjs.NewSceneCode.GDnumberObjects3.length = 0;
gdjs.NewSceneCode.GDnumberObjects4.length = 0;
gdjs.NewSceneCode.GDnumber2Objects1.length = 0;
gdjs.NewSceneCode.GDnumber2Objects2.length = 0;
gdjs.NewSceneCode.GDnumber2Objects3.length = 0;
gdjs.NewSceneCode.GDnumber2Objects4.length = 0;
gdjs.NewSceneCode.GDprutya_95horizontalObjects1.length = 0;
gdjs.NewSceneCode.GDprutya_95horizontalObjects2.length = 0;
gdjs.NewSceneCode.GDprutya_95horizontalObjects3.length = 0;
gdjs.NewSceneCode.GDprutya_95horizontalObjects4.length = 0;
gdjs.NewSceneCode.GDprutya_95verticalObjects1.length = 0;
gdjs.NewSceneCode.GDprutya_95verticalObjects2.length = 0;
gdjs.NewSceneCode.GDprutya_95verticalObjects3.length = 0;
gdjs.NewSceneCode.GDprutya_95verticalObjects4.length = 0;
gdjs.NewSceneCode.GDtumbler1Objects1.length = 0;
gdjs.NewSceneCode.GDtumbler1Objects2.length = 0;
gdjs.NewSceneCode.GDtumbler1Objects3.length = 0;
gdjs.NewSceneCode.GDtumbler1Objects4.length = 0;
gdjs.NewSceneCode.GDtumbler2Objects1.length = 0;
gdjs.NewSceneCode.GDtumbler2Objects2.length = 0;
gdjs.NewSceneCode.GDtumbler2Objects3.length = 0;
gdjs.NewSceneCode.GDtumbler2Objects4.length = 0;
gdjs.NewSceneCode.GDtumbler3Objects1.length = 0;
gdjs.NewSceneCode.GDtumbler3Objects2.length = 0;
gdjs.NewSceneCode.GDtumbler3Objects3.length = 0;
gdjs.NewSceneCode.GDtumbler3Objects4.length = 0;
gdjs.NewSceneCode.GDupObjects1.length = 0;
gdjs.NewSceneCode.GDupObjects2.length = 0;
gdjs.NewSceneCode.GDupObjects3.length = 0;
gdjs.NewSceneCode.GDupObjects4.length = 0;
gdjs.NewSceneCode.GDup2Objects1.length = 0;
gdjs.NewSceneCode.GDup2Objects2.length = 0;
gdjs.NewSceneCode.GDup2Objects3.length = 0;
gdjs.NewSceneCode.GDup2Objects4.length = 0;
gdjs.NewSceneCode.GDwheelObjects1.length = 0;
gdjs.NewSceneCode.GDwheelObjects2.length = 0;
gdjs.NewSceneCode.GDwheelObjects3.length = 0;
gdjs.NewSceneCode.GDwheelObjects4.length = 0;

gdjs.NewSceneCode.eventsList0xb2358(runtimeScene);
return;
}
gdjs['NewSceneCode'] = gdjs.NewSceneCode;
