
function gravitySpeedUpdate(gx,gy,G,bodies,interval) {
    bodies.forEach(
        (element) => {
            var distance = Math.hypot(gx-element.x,gy-element.y);
            if (distance > 25) {
                var dv = G*interval/(distance);
                var c = [(gx-element.x)/distance,(gy-element.y)/distance];
                var dvx = c[0]*dv, dvy = c[1]*dv;
                element.vx += dvx;
                element.vy += dvy;
            }
        }
    );
}

function makeControlledGravitySpeedUpdate(controller) {
    return (bodies,interval) => {
        if (controller.ask(interval)) {
            gravitySpeedUpdate(
                controller.gravityCenter.x,controller.gravityCenter.y,controller.gravityCenter.G
                ,bodies,interval);    
        }
    }
}

function GravityCenter(x,y,G) {
    this.x = x;
    this.y = y;
    this.G = G;
}

function GravityController(gravityCenter,duration) {
    this.gravityCenter = gravityCenter;
    this.active = false;
    this.timeLapsed = 0;
    this.ask = function (interval) {
        if (!this.activate) return false;
        if (this.active && (this.timeLapsed += interval) <= duration)
            return true;
        this.active = false;
        return false;
    }
    this.activate = function() {
        this.timeLapsed = 0;
        this.active = true;
    }
}

var GravityState = {IDLE: "idle", START: "start", COLLAPSE: "collapse"};

function GravityCircleController(collapseRadius, collapseDuration) {
    this.currentState = GravityState.IDLE;
    this.collapseRadius = collapseRadius
    this.currentRadius = 0;
    this.collapseDuration = collapseDuration;
    this.x = 0;
    this.y = 0;
}

function GravityCircle(context2D,controller) {
    var timeLapsed = 0;
    return new GameObject(
        //update
        interval => {
            if (GravityState.IDLE == controller.currentState) return;
            if (GravityState.START == controller.currentState) {
                timeLapsed = 0;
                controller.currentState = GravityState.COLLAPSE;
                return;
            }
            if ((timeLapsed += interval) > controller.collapseDuration) {
                timeLapsed = controller.collapseDuration;
                controller.currentState = GravityState.IDLE;
                return;
            }
            controller.currentRadius = controller.collapseRadius - controller.collapseRadius*timeLapsed/controller.collapseDuration;
        },
        //draw
        () => {
            if (GravityState.IDLE == controller.currentState) return;
            if (GravityState.COLLAPSE == controller.currentState) {
                context2D.save();
                context2D.setTransform(1,0,0,1,controller.x,controller.y);
                context2D.beginPath();
                context2D.arc(0, 0, controller.currentRadius, 0, 2 * Math.PI);
                context2D.lineWidth = 2;
                context2D.strokeStyle = "#33cccc";
                context2D.stroke();
                context2D.restore();
            }

        }
    );
}