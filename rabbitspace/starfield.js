
function Star(x,y,style,size,velocity) {
    this.x = x;
    this.y = y;
    this.style = style;
    this.size = size;
    this.velocity = velocity;
}

function makeStarDraw(star,context2D) {
    return () => {
        var oldFillStyle = context2D.fillStyle;
        context2D.fillStyle = star.style;
        context2D.beginPath();
        context2D.arc(star.x, star.y, star.size, 0, 2 * Math.PI);
        context2D.fill();
        context2D.fillStyle = oldFillStyle;
    };
}

function makeStarUpdate(star,boundary) {
    var timeLapsed = 0;
    return interval => {
        timeLapsed += interval;
        var pixels = star.velocity*timeLapsed/1000;
        if (pixels > 0) {
            star.y += pixels;
            timeLapsed = timeLapsed%1000/star.velocity;
            boundary(star);
        }
    };
}

function makeStar(x,y,style,size,velocity,context2D,boundary) {
    var star = new Star(x,y,style,size,velocity);
    return new GameObject(
        makeStarUpdate(star,boundary)
        , makeStarDraw(star,context2D)
    );
}

function testBoundary(star) {
    if (star.y > 800+star.size) {
        star.y = -star.size;
        star.x = Math.floor(Math.random()*(595-5)+5);
        star.style = ["#FFDDDD","#DDDDFF","#FFBBBB","#EEEEEE"][Math.floor(Math.random()*3)];
        star.size = Math.floor(Math.random()*(4-2)+2);
        star.velocity = Math.floor(Math.random()*(10-5)+10);
    }
}

function randomStar(context2D,boundary) {
    return makeStar(
          Math.floor(Math.random()*(595-5)+5)
        , Math.floor(Math.random()*(795-5)+5)
        , [ "#FFDDDD","#DDDDFF","#FFBBBB","#EEEEEE"][Math.floor(Math.random()*3)]
        , Math.floor(Math.random()*(4-2)+2)
        , Math.floor(Math.random()*(10-5)+10) 
        , context2D
        , boundary
    );
}


