function Asteroid(spriteSrc,transform,x,y,vx,vy,a,w) {
    this.spriteSrc = spriteSrc;
    this.transform = transform;
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
    this.a = a;
    this.w = w;
}

function makeAsteroidDraw(context2D,asteroid) {
    return makeSpriteDraw(context2D,asteroid.spriteSrc,asteroid.transform);
}

function makeAsteroidUpdate(asteroid) {
    var timeLapsedX = 0, timeLapsedY = 0;
    return interval => {
        timeLapsedX += asteroid.vx*interval;
        timeLapsedY += asteroid.vy*interval;
        var pixelsX = timeLapsedX/1000;
        var pixelsY = timeLapsedY/1000;

        if (pixelsX >= 1 || pixelsX <= -1) {
            asteroid.x += pixelsX;
            timeLapsedX -= pixelsX*1000;
         }

        if (pixelsY >= 1 || pixelsY <= -1) {
            asteroid.y += pixelsY;
            timeLapsedY -= pixelsY*1000;
        }

        asteroid.a += asteroid.w*interval/1000;

        compositeTransform(
            rotateTransform(asteroid.a,asteroid.spriteSrc.sw/2,asteroid.spriteSrc.sh/2)
            ,translateTransform(asteroid.x-asteroid.spriteSrc.sw/2,asteroid.y-asteroid.spriteSrc.sh/2)
        ).forEach((element,idx) => {asteroid.transform[idx] = element;});
    };
}

function setAsteroidRandom(asteroid) {
    asteroid.vx = randomInt(25,65);
    asteroid.vy = 0;
    asteroid.a = Math.random()*(2*Math.PI);
    asteroid.w = Math.random()*(Math.PI/4)-Math.PI/8;
}

function makeAsteroidBoundaryCheck(boundRect) {
    return asteroid => {
        if (asteroid.y < boundRect[1]-asteroid.spriteSrc.sh/2) return 1;
        if (asteroid.x < boundRect[0]-asteroid.spriteSrc.sw/2) return 2;
        if (asteroid.y > boundRect[1]+boundRect[3]+asteroid.spriteSrc.sh/2) return 3;
        if (asteroid.x > boundRect[0]+boundRect[2]+asteroid.spriteSrc.sw/2) return 4;
        return 0;
    }
}

function asteroidsDistance(a1,a2) {
    return Math.hypot(a2.x-a1.x,a2.y-a1.y);
}

function asteroidsCollide(a1,a2) {
    var d = asteroidsDistance(a1,a2);
    var c = [(a2.x-a1.x)/d,(a2.y-a1.y)/d];
    var scalar1 = c[0]*a1.vx+c[1]*a1.vy;
    var scalar2 = c[0]*a2.vx+c[1]*a2.vy;
    a1.vx += scalar2*c[0] - scalar1*c[0]; a1.vy += scalar2*c[1] - scalar1*c[1];
    a2.vx += scalar1*c[0] - scalar2*c[0]; a2.vy += scalar1*c[1] - scalar2*c[1];
    var center = [(a2.x+a1.x)/2.,(a2.y+a1.y)/2.];
    a1.x = center[0]-c[0]*38; a1.y = center[1]-c[1]*38;
    a2.x = center[0]+c[0]*38; a2.y = center[1]+c[1]*38;
}

function AsteroidVacancy(rect,asteroids) {
    this.rect = rect;
    this.vacant = true;
    this.update = function () {
        this.vacant = asteroids.every(
            a => {
                return a.x < rect[0]-a.spriteSrc.sw/2 || a.y < rect[1]-a.spriteSrc.sh/2 || a.x > rect[0]+rect[2]+a.spriteSrc.sw/2 || a.y > rect[1]+rect[3]+a.spriteSrc.sh/2;
            }
        );
    }
}

function makeAsteroidField(asteroids,context2D,additionalUpdate) {
    var vacancies = [
          new AsteroidVacancy([0,325,150,150],asteroids)
        , new AsteroidVacancy([0,150,150,150],asteroids)
        , new AsteroidVacancy([0,500,150,150],asteroids)
    ];
    var field = (new Array(asteroids.length)).fill(false);
    var drawFuncs = asteroids.map((a)=>{ return makeAsteroidDraw(context2D,a); });
    var updateFuncs = asteroids.map((a) => { return makeAsteroidUpdate(a); });
    var asteroidBoundaryCheck = makeAsteroidBoundaryCheck([0,0,600,800]);
    var presentAsteroidsCount = 0;
    return new GameObject(
        interval => {
            //update vacancies
            vacancies.forEach(v => v.update());
            for (var i =0; i != vacancies.length; ++i) {
                if (presentAsteroidsCount == field.length) break;
                if (!vacancies[i].vacant) continue;
                var asteroidPosition = 0;
                while (field[asteroidPosition])
                    ++asteroidPosition;
                
                setAsteroidRandom(asteroids[asteroidPosition]);
                asteroids[asteroidPosition].x = vacancies[i].rect[0]-asteroids[asteroidPosition].spriteSrc.sw/2;
                asteroids[asteroidPosition].y = vacancies[i].rect[1]+asteroids[asteroidPosition].spriteSrc.sh/2;
                
                field[asteroidPosition] = true;
                vacancies[i].vacant = false;
                ++presentAsteroidsCount;
            }
            
            field.forEach((e,idx) => { if(e) { updateFuncs[idx](interval); } });
            field.forEach(
                (e,idx) => { 
                    if(e && asteroidBoundaryCheck(asteroids[idx]) > 0) { 
                        field[idx] = false;
                        --presentAsteroidsCount;
                        asteroids[idx].x = -300;
                        asteroids[idx].y = -300;
                        asteroids[idx].vx = 0;
                        asteroids[idx].vy = 0;
                    } 
                }
            );
            
            //asteroids collisions
            var size = field.length;
            for (var i = 0; i != size-1; ++i)
                for (var j = i+1; j != size; ++j)
                    if (field[i] && field[j] && asteroidsDistance(asteroids[i],asteroids[j]) < 75) {
                        asteroidsCollide(asteroids[i],asteroids[j]);
                    }

            //additional update like gravitational field
            additionalUpdate(asteroids.filter( (e,i) => { return field[i];} ), interval);
        },
        () => {
            field.forEach((e,idx)=>{ if(e){ drawFuncs[idx](); } });
        }
    );
}