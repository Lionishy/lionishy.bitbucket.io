/**
 * Wrapper for the Image to further loading
 * Useful in loading multiple images
 * 
 * @param {String} source - the source URL of the images to be loaded
 */
function PreparedImage(source) {
    this.image = new Image();
    this.source = source;
    this.isLoaded = false;
    this.load = function(doOnLoad) {
        var preparedImage = this;
        this.image.onload = function() {
            preparedImage.isLoaded = true;
            doOnLoad(preparedImage);
        }
        this.image.src = this.source;
    }
}

/**
 * Function to facilitate load of multiple images
 * Takes an array of images to be loaded and call the function specified when all images have been loaded
 * 
 * @param {()=>undefined} next                  - a function to be called on all images been loaded
 * @param {Array<PreparedImage>} preparedImages - an array of the PreparedImage objects to be loaded
 */
function loadPreparedImages(next,preparedImages) {
    //function to be called on image loaed to check for all images done if so - calls next
    var onImageLoaded = function(preparedImage) { 
        if ( preparedImages.every( function(preparedImage) {return preparedImage.isLoaded;} ) ) {
            return next();
        } 
    }
    
    //dispatch loading
    preparedImages.forEach(
        function(preparedImage) {
            preparedImage.load(onImageLoaded);
        }
    );
}

/**
 * Special type of Exception with two messages: to alert user and to log for debug
 * 
 * @param {String} userMessage - message to be shown to the user 
 * @param {String} logMessage  - message to to be logged in debug
 */
function UserAwareException(userMessage,logMessage) {
    this.userMessage = userMessage;
    this.logMessage = logMessage;
}

/**
 * Constructs a function equivalent to the window.requestAnimationFrame
 * In case of failure throws a UserAwareException
 * 
 * @returns {( (interval)=>undefined )=>long} - a function equivalent the window.requestAnimationFrame chosen for specific browser
 * @throws {UserAwareException} - in case no function equivalent is found
 */
function makeRequestAnimationFrame() {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    if (!requestAnimationFrame) 
        throw new UserAwareException(
            "Игра не поддерживается вашим браузером :( Извините"
            , "Browser does not suppor request animation frame technology");
    return requestAnimationFrame;
}

function randomInt(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}