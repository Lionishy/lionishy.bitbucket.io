/**
 * A GameObject represents an object in the game with its own state to be updated via 'update' function-member
 * and its representation to be drawn via 'draw' function-member  
 * 
 * @param {interval => undefined} update - a function to be called in main update loop
 * @param {() => undefined } draw        - a function to be called in main animation recursion
 */
function GameObject(update,draw) {
    this.update = update;
    this.draw = draw;
}

/**
 * 
 * @param {Array<GameObject>} 
 */
function Scene(gameObjects) {
    return (interval) => {
        gameObjects.forEach(obj => obj.update(interval));
        gameObjects.forEach(obj => obj.draw());
    };
}

/***
  * The source image of the sprite
  * Every sprite block holds information about the source image - 
  * image object, top-left corner postion and widht|height
  * 
  * @param { Image } image - an Image() object
  * @param { Int }   sx    - x coordinate of the crop rectangle top-left corner inside the source image
  * @param { Int }   sy    - y coordintae of the crop rectangle top-left corner inside the source image
  * @param { Int }   sw    - crop rectangle width
  * @param { Int }   sh    - crop rectangle height
  * */
function SpriteSrc(image,sx,sy,sw,sh) {
    this.image = image;
    this.sx = sx;
    this.sy = sy;
    this.sw = sw;
    this.sh = sh;
}

/**
 * Creates a function to draw a source image represented by the SpriteSrc object
 * to the given canvas context with transformation applied
 * 
 * @param {CanvasRenderingContext2D} context2D 
 * @param {SpriteSrc} spriteSrc 
 * @param {Array<Float>} transform 
 */
function makeSpriteDraw(context2D,spriteSrc,transform) {
    return () => {
        context2D.save();
        context2D.setTransform(
            transform[0],transform[1]
            ,transform[2],transform[3]
            ,transform[4],transform[5]);
        context2D.drawImage(
            spriteSrc.image,spriteSrc.sx,spriteSrc.sy,spriteSrc.sw,spriteSrc.sh
            ,0,0,spriteSrc.sw,spriteSrc.sh);
        context2D.restore();
    };
}

function makeAnimatedSprite(context2D,transform,sprites,duration,loop,update) {
    var timeLapsed = 0;
    var frameInterval = duration/sprites.length;
    var frameLapsed = 0;
    var frameCurrent = 0;
    var bounce = 1;
    return new GameObject(
        interval => {
            if (timeLapsed > duration && (0 != loop)) timeLapsed -= duration;
            if (timeLapsed >= duration) return;
            timeLapsed += interval;

            frameLapsed += interval;
            if (frameLapsed > frameInterval) {
                frameLapsed -= frameInterval;
                switch(loop) {
                case 1:
                    frameCurrent = (frameCurrent+1)%sprites.length;
                    break;

                case 2:
                    frameCurrent += bounce;
                    if (0 == frameCurrent || (sprites.length-1) == frameCurrent) bounce *= -1;
                    break;

                default:
                    ++frameCurrent;
                }
            }

            update(interval);
        }
        , () => {
            context2D.save();
            context2D.setTransform(
                transform[0],transform[1]
                ,transform[2],transform[3]
                ,transform[4],transform[5]);
            var spriteSrc = sprites[frameCurrent];
            context2D.drawImage(
                spriteSrc.image,spriteSrc.sx,spriteSrc.sy,spriteSrc.sw,spriteSrc.sh
                ,0,0,spriteSrc.sw,spriteSrc.sh);
            context2D.restore();
        }
    );
}

/**
  * The function to compose two transformations into one
  * The arguments order matters!
  * 
  * @param {Array<Float>} first  - the transformation to be applied first
  * @param {Array<Float>} second - the transformation to be applied second
  * 
  * @returns {Array<Float>} new transformation
  * */
 function compositeTransform(first,second) {
    return [
          second[0]*first[0] + second[2]*first[1]
        , second[1]*first[0] + second[3]*first[1]
        , second[0]*first[2] + second[2]*first[3]
        , second[1]*first[2] + second[3]*first[3]
        , second[0]*first[4] + second[2]*first[5] + second[4]
        , second[1]*first[4] + second[3]*first[5] + second[5]
    ];
 }

 /**
  * The function to prepare a translate transformation
  * 
  * @param {Float} dx - translate along X-axis 
  * @param {Float} dy - translate along Y-axis
  * 
  * @returns {Array<Float>} new transformation
  * */
 function translateTransform(dx,dy) {
     return [1,0,0,1,dx,dy];
 }

 /**
  * The function to prepare a rotation transformation
  * 
  * @param {Float} angle - an angle to rotate 
  * @param {Float} cx    - x coordinate of the point to rotate about
  * @param {Float} cy    - y coordinate of the point to rotate about 
  * 
  * @returns { Array<Float> } new transformation
  * */
function rotateTransform(angle,cx,cy) {
    return compositeTransform(
         [1,0,0,1,-cx,-cy]
        , compositeTransform(
              [Math.cos(angle),Math.sin(angle),-Math.sin(angle),Math.cos(angle),0,0]
            , [1,0,0,1,cx,cy]
        )
    );
}