/**
 * The Screen object is the abstraction of the screen to be drawn on the canvas
 * The Screen can be drawn with the Screen.draw() function {()=>Screen}
 * The Screen can be updated with the Screen.update(timestamp) function {timestamp => Screen}
 * At first animation_loop calls the .draw() function than on the Screen returned it calls .update(timestamp)
 * Each Screen object is transferred to another call of the requestAnimation function
 * 
 * @param {Object} screen 
 * @param {(interval => Screen) => undefined} requestAnimation 
 */
function animation_loop(screen,requestAnimation) {
    return timestamp => {
        return requestAnimation(animation_loop(screen.draw().update(timestamp),requestAnimation));
    }
}